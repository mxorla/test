
//DB Migration
var DBMigrate = require('db-migrate');

//getting an instance of dbmigrate
var dbmigrate = DBMigrate.getInstance(true, {
    env: 'dev'
});

const run = () => {
    //execute any of the API methods
    dbmigrate.up();
};


module.exports.run = run;