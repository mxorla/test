CREATE TABLE IF NOT EXISTS `invites` (
    id int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
    user_id int(11) NOT NULL,
    datetime DATETIME NOT NULL,
    email_invited VARCHAR(150) NOT NULL,
    description VARCHAR(512),
    tolerance VARCHAR(10),
    phone varchar(15),
    status varchar(15) DEFAULT 'CREATED',
    hashvalue varchar(512),
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users(id)
);