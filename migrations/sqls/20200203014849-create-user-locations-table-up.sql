CREATE TABLE IF NOT EXISTS `user_locations` (
    id int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
    user_id int(11),
    lat DECIMAL(10, 8) NOT NULL, 
    lng DECIMAL(11, 8) NOT NULL ,
    favorite BOOLEAN,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users(id)
);