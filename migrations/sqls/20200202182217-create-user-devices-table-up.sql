CREATE TABLE IF NOT EXISTS `user_devices` (
    id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
    user_id INT(11) NOT NULL,
    push_id VARCHAR(512) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    UNIQUE KEY `push_id` (`push_id`),
    FOREIGN KEY (user_id) REFERENCES users(id)
);
