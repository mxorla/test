CREATE TABLE IF NOT EXISTS `user_in_roles` (
    id int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
    user_id int(11),
    role_id int(11),
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (role_id) REFERENCES roles(id)
);

INSERT INTO user_in_roles (user_id, role_id) VALUES
((select id from users limit 1), (select id from roles where name='HOST'));