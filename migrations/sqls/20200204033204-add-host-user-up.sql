INSERT INTO users (name, lastname, phone, postal_code, age, gender, email, username, password) VALUES
('Marcelo', 'Burriel', '2215380324', 1900, 30, 'M', 'marceloburriel@gmail.com', 'marceloburriel@gmail.com', '$2a$10$81xMt1jtXZSjN05/.fhKze/Zy7Hd42eC/C1onNdxl6aZ16bv4mm.C');

INSERT INTO user_in_roles (user_id, role_id) VALUES
((select id from users where username='marceloburriel@gmail.com'), (select id from roles where name='HOST'));