CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `name` varchar(50) NOT NULL COMMENT 'user namme',
  `lastname` varchar(100) NOT NULL COMMENT 'user lastname',
  `phone` varchar(15) NOT NULL COMMENT 'cellphone',
  `postal_code` int(5) DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `gender` char(1) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`),
  KEY `name` (`name`),
  FULLTEXT KEY `lastname` (`lastname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='users table';

INSERT INTO users (name, lastname, phone, postal_code, age, gender, email, username, password) VALUES
('Matias', 'Orlandella', '2216020263', 1900, 30, 'M', 'matias.orlandella@gmail.com', 'matias.orlandella@gmail.com', '$2a$10$81xMt1jtXZSjN05/.fhKze/Zy7Hd42eC/C1onNdxl6aZ16bv4mm.C');
