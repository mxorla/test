module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  settings: {
    react: {
      version: "latest"
    }
  },
  parserOptions: {
    ecmaFeatures: {
      ecmaVersion: 2018,
      jsx: true,
    },
    ecmaVersion: 2020,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  ignorePatterns: ["temp.js", "node_modules/"],
  rules: {
    "react/prop-types": [0, { ignore: ['children'] }],
    'react-hooks/rules-of-hooks': 'off', // Checks rules of Hooks
    'react-hooks/exhaustive-deps': 'off', // Checks effect dependencies
    'no-invalid-this': 'off',
    'react/jsx-uses-react': 1,
    'react/jsx-uses-vars': 1,
    'no-constant-condition': 2,
    'no-irregular-whitespace': 2,
    'no-multi-spaces': 2,
    'no-lonely-if': 0,
    'no-plusplus': 0,
    "block-spacing": ["error", "always"],
    'no-mixed-spaces-and-tabs': 2,
    'jsx-quotes': [2, "prefer-single"],
    "comma-spacing" : ["error", { "before": false, "after": true }],
    "object-curly-spacing": ["error", "always"],
    "array-bracket-spacing" : ["error", "always"],
    "indent": ["error", 4 ],
    "semi": ["error", "always"],
    "quotes": ["error","single"],
  },
};
