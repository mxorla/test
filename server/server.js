require('dotenv').config();


const express = require('express');
const app = express();
const openRoutes = require('./routes/open');
const securedRoutes = require('./routes/secured');
//Authentication and Authorization Middleware
const { protectedRoute } = require('./services/auth.service');
const bodyParser = require('body-parser');
const requestId = require('express-request-id')();

const firebase = require('./external-services/firebase.service');
firebase.sendPush('fyMRVfJyOPk:APA91bFzOXv__XQ0vUIvVU24vMrz0AGIciRSk6ZA28Rymc9k3_6QspoEfNa3S81dW4wR0c3Wz0pLETFMMxo0zgoQafB5Ywsy_xBmLzVry1vvKR0wxGja3vwO77YmGvutxCetY_eV3ogd', 'titulo', 'marce largame un apk');
// const migrate = require('../migrations/config/migrate');
// migrate.run();

app.use(requestId);
/* Create the 'id' token in morgan that returns the Request ID. 
Refer https://www.npmjs.com/package/morgan#tokens for more details. 
*/

//Static routes configuration for ReactJS app
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


//Not secured routes
app.use('/', openRoutes);
//Secured routes
app.use(protectedRoute);
app.use('/', securedRoutes);


const port = process.env.PORT || 3000;
console.log(`App listening on port: ${port}`);
app.listen(port);


