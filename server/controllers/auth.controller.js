const authService = require('../services/auth.service');

class AuthController {
    async post(req, res) {
        try {
            const username = req.body.user;
            const pushId = req.body.pushId;
            const pass = req.body.pass;
            const resp = await authService.login({ username, pushId }, pass);
            res.json(resp);
        } catch (error) {
            let errorResp = { message: error.message };
            res.status(401).json(errorResp);
        }
    }
}

module.exports = new AuthController;

