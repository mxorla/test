const userService = require('../services/user.service');

class UserController {

    async getUser(req, res) {
        try {
            const resp = await userService.getUser(req.params.username);
            res.json(resp);
        } catch (error) {
            let errorResp = { message: error.message };
            res.status(400).json(errorResp);
        }
    }

    async setGeolocation(req, res) {
        try {
            const { lat, lng } = req.body;
            if (!lat || !lng)
                throw new Error('Invalid request');
            await userService.setGeolocation(req.decoded.username, req.body);
            res.end();
        } catch (error) {
            let errorResp = { message: error.message };
            res.status(400).json(errorResp);
        }
    }

    async register(req, res) {
        try {
            const resp = await userService.createUser(req.body);
            res.json(resp);
        } catch (error) {
            let errorResp = { message: error.message };
            res.status(400).json(errorResp);
        }
    }

    async validateAccount(req, res) {
        try {
            if (!req.query.hash)
                throw new Error('Invalid request');

            const resp = await userService.validateAccount(req.query.hash);
            res.json(resp);
        } catch (error) {
            let errorResp = { message: error.message };
            res.status(400).json(errorResp);
        }
    }
}

module.exports = new UserController;

