const inviteService = require('../services/invite.service');

class InviteController {

    async addInvite(req, res) {
        try {
            const { email, datetime } = req.body;
            if (!(email && datetime))
                throw Error('invalid request');
            const resp = await inviteService.addInvite(req.decoded.username, req.body);
            res.json(resp);
        } catch (error) {
            let errorResp = { message: error.message };
            res.status(400).json(errorResp);
        }
    }


}

module.exports = new InviteController;

