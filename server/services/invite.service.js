const Invite = require('../model').Invite;
const notificationService = require('./notification.service');


class InviteService {
    async addInvite(username, data) {
        const { email } = data;
        const { invite: { affectedRows, insertId }, hashvalue } = await Invite.saveInvite(username, data);
        let response = { success: affectedRows > 0, insertId };
        if (response.success) {
            const { MessageId } = await notificationService.sendEmail(email, 'Invitacion', '../templates/created-invite.template.html', [{ url: `webalformulario/${Buffer.from(hashvalue).toString('base64')}` }]);
            if (MessageId) {
                const { invite: { affectedRows } } = await Invite.changeStatus(insertId);
                response = { ...response, success: affectedRows > 0, email_sent_success: !!MessageId };
            }
        }

        return response;
    }
}

module.exports = new InviteService;