const routes = {
    CLIENT: [
        { regex: /^\/api\/calendar$/, methods: [ 'GET' ] },
        { regex: /^\/api\/calendar\/appointments[\/\d]*$/, methods: [ 'GET', 'POST', 'PUT', 'DELETE' ] },
        { regex: /^\/api\/calendar\/workingHours[\/\d]*$/, methods: [ 'GET' ] },        
    ],
    PSA: [
        { regex: /^\/api\/calendar\/appointments[\/\d]*$/, methods: [ 'GET', 'DELETE' ] }
    ],
    HOST: [
        { regex: /^\/api\/users\/geolocation[\/\d]*$/, methods: [ 'POST' ] },
        { regex: /^\/api\/invite[\/\d]*$/, methods: [ 'POST' ] }

    ]
};

module.exports = routes;