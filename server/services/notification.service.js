const SESExternalService = require('../external-services/ses.service');
const fs = require('fs');
const path = require('path');

class NotificationService {
    async sendEmail(to, subject, templatePath, values = []) {
        const template = await fs.readFileSync(path.join(__dirname, templatePath), 'utf8');
        let body = template;
        values.forEach((value) => {
            const k = Object.keys(value)[0];
            const v = value[k];
            body = template.replace(new RegExp(`{${k}}`, 'g'), v);
        });
        return await SESExternalService.sendEmail(to, subject, body);
    }
}

module.exports = new NotificationService;