const superagent = require('superagent');

class HttpService {
    constructor(options) {

    }

    post(options) {
        options.method = 'POST';
        return this.request(options);
    }

    get(options) {
        options.method = 'GET';
        return this.request(options);
    }

    put(options) {
        options.method = 'PUT';
        return this.request(options);
    }

    patch(options) {
        options.method = 'PATCH';
        return this.request(options);
    }

    delete(options) {
        options.method = 'DELETE';
        return this.request(options);
    }

    async _request(options) {
        this.logRequest(
            options.url,
            options.method,
            options.body,
            options.serviceName
        );


        const header = Object.assign(
            this.getDefaultHeader(options.method),
            options.header
        );

        let builder = superagent(options.method, options.url)
            .set(header) // For header
            .timeout((options.timeout || 30000)); // 30 seg by default


        if (options.attach) {
            builder.attach(
                options.attach.field,
                options.attach.file.buffer,
                options.attach.options
            );
        }

        if (options.type) {
            builder.type(options.type);
        }

        if (options.queryParams) {
            builder = builder.query(options.queryParams); // For Query
        }

        if (!options.attach &&
            ['POST', 'PUT', 'PATCH'].includes(options.method)) {
            builder = builder.send(options.body); // For Bodys
        }

        const res = await builder;

        this.logResponse(
            options.url,
            options.method,
            options.serviceName,
            res
        );

        return res;
    }

    async request(options) {
        try {
            const response = await this._request(options);

            return response;
        } catch (err) {
            const jsonError = JSON.parse(err.response.error.text);
            if (jsonError.response.error || jsonError.response.message)
                throw new Error(jsonError.response.error ? jsonError.response.error : jsonError.response.message)
        }
    }

    logRequest(url, method, data, serviceMethodName) {

    }

    logResponse(url, method, serviceMethodName, res) {

    }

    getLogMeta(url, method, serviceMethodName) {
        return {
            method,
            serviceName: serviceMethodName,
            uri: url,
            layer: 'infrastructure',
        };
    }

    getDefaultHeader(method) {
        const header = {
            charset: 'utf-8',
            Accept: 'application/json',
        };

        if (['POST', 'PUT', 'PATCH'].includes(method)) {
            header['Content-Type'] = 'application/json';
        }

        return header;
    }
}

module.exports = HttpService;
