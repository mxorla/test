const notificationService = require('./notification.service');
const authService = require('./auth.service');
const moment = require('moment');
const User = require('../model').User;
const Role = require('../model').Role;
const bcrypt = require('bcryptjs');
const saltRounds = 10;


class UserService {
    async getUser(username) {
        const user = await User.findByUsername(username);
        if (!user)
            throw Error('Invalid hash');

        const [devices, locations] = await Promise.all([User.getDevices(username), User.getLocations(username)]);

        user['devices'] = devices;
        user['location'] = locations.length ? { lat: locations[0].lat, lng: locations[0].lng } : undefined;
        return user;
    }

    async setGeolocation(username, geolocation) {
        const { lat, lng } = geolocation;
        await User.setGeolocation(username, lat, lng);
    }

    async createUser(data) {
        const emailIsRegistered = await User.exists({ email: new RegExp(data.email, 'i') });
        if (emailIsRegistered)
            throw Error('Email already is registered');

        const role = await Role.findOne({ name: 'CLIENT' });
        const { firstname, lastname, password, email } = data;
        const [hashedPassword, hashedAccount] = await Promise.all([
            bcrypt.hash(password, saltRounds),
            bcrypt.hash(firstname + lastname + email, saltRounds)]);
        const user = new User({
            username: email,
            firstname,
            lastname,
            password: hashedPassword,
            email,
            role,
            loginAttempts: 0,
            blocked: false,
            accountValidated: false,
            hashedAccount,
            createdAt: moment().utc()
        });

        // save model to database
        const addedUser = await user.save();

        // if (err) return console.error(err);
        notificationService.sendEmail(addedUser.email, 'Validate account', '../templates/approved-invite.template.html', [{ url: `${process.env.ELO_HOST}?hash=${encodeURIComponent(addedUser.hashedAccount)}` }]);
        return addedUser;

    }

    async validateAccount(hashedAccount) {
        const user = await User.findOne({ hashedAccount: decodeURIComponent(hashedAccount), accountValidated: false });
        if (!user)
            throw Error('Invalid hash');

        const duration = moment.duration(moment().utc().diff(user.createdAt));
        const minutes = duration.asMinutes();

        if (process.env.HASH_ACCOUNT_EXPIRATION_TIME && minutes > parseInt(process.env.HASH_ACCOUNT_EXPIRATION_TIME))
            throw Error('Expired hash');

        await user.updateOne({ accountValidated: true });

        return await authService.login(user);
    }
}

module.exports = new UserService;