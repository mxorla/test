const jwt = require('jsonwebtoken');
const express = require('express');
const routes = require('./auth.routes');
const User = require('../model').User;
// const Role = require('../model').Role;
const bcrypt = require('bcryptjs');

//User Authentication
const login = async (userinfo, password = '') => {
    const { username, hashedAccount, pushId } = userinfo;
    const user = await User.findByUsername(username);
    const hash = user ? user.password : '';
    const passwordOk = await bcrypt.compare(password, hash);

    if ((!passwordOk || !user) && (!hashedAccount || (hashedAccount != user.hashedAccount))) {
        // if (!passwordOk && user)
        //     await user.update({ loginAttempts: user.loginAttempts + 1 }); // TODO: Add block policy
        throw Error('Username or password are wrong');
    }

    User.upsertPushId(username, pushId);
    const { name, lastname, email, role } = user;
    const payload = {
        username: user.username,
        name,
        lastname,
        email,
        role
    };

    if (payload) {
        const token = jwt.sign(payload, process.env.JWT_KEY, {
            expiresIn: parseInt(process.env.tokenExpiresIn)
        });
        var resp = {
            mensaje: 'Successful Authentication',
            token: token
        };
        return resp;
    } else {
        throw Error('Username or password are wrong');
    }
};

const protectedRoute = express.Router();
//Routes protection middleware
protectedRoute.use((req, res, next) => {
    var token = req.headers['authorization'];
    token = token == null ? null : token.replace('Bearer ', '');
    //Verify if token exits
    if (token) {
        jwt.verify(token, process.env.JWT_KEY, (err, decoded) => {
            //Verify if token is valid
            if (err) {
                return res.json({ mensaje: 'Invalid token' });
            } else {
                //Verify if route is allowed
                if (
                    routes[decoded.role].find(function (route) {
                        return route.regex.test(req.path) && route.methods.includes(req.method);
                    })
                ) {
                    req.decoded = decoded;
                    //TODO look for user in BD and add to req
                    next();
                } else {
                    res.status(403).send({
                        mensaje: 'Invalid Role'
                    });
                }
            }
        });
    } else {
        res.status(401).send({
            mensaje: 'Invalid Token'
        });
    }
});

module.exports.protectedRoute = protectedRoute;
module.exports.login = login;
