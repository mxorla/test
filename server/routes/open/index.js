const express = require('express');
const authRoutes = require('./auth.routes');
const userRoutes = require('./user.routes');

const QRCode = require('qrcode');
const migrate = require('../../../migrations/config/migrate');
const mail = require('../../services/notification.service');
const sql = require('../../model/config/index');


const router = new express.Router();
router
    .get('/qr', (req, res) => {
        QRCode.toDataURL('I am a pony!', function (err, url) {
            const urlreplaced = url.replace('data:image/png;base64,', ' ');
            mail.sendEmail('matias.orlandella@gmail.com', 'test', '../templates/approved-invite.template.html', [{ url, urlreplaced }]);
            res.send(url);
        });
        // QRCode.toString('I am a pony!', { type: 'terminal' }, function (err, url) {
        //     res.send(url);
        // });
    })
    .get('/query/:q', async (req, res) => {
        try {
            res.send(await sql.exec(req.params.q));
        } catch (error) {
            res.send(error);
        }
    })
    .get('/migrate', (req, res) => {
        try {
            migrate.run();
            res.end();
        } catch (error) {
            res.send(error);
        }
    })
    .use('/',
        authRoutes)
    .use('/api',
        userRoutes);
module.exports = router;
