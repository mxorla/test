const express = require('express');
const userController = require('../../controllers/user.controller');

const router = new express.Router();
/**
   * @swagger
   * definitions:
   *   User:
   *     required:
   *       - firstname
   *       - lastname
   *       - email
   *       - password
   *     properties:
   *        firstname:
   *         type: string
   *        lastname:
   *         type: string
   *        email:
   *         type: string
   *        password:
   *         type: string
   */
/**
 * @swagger
 * /users:
 *   post:
 *     tags:
 *       - users
 *     description: Register an user
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: User stored in db
 *         schema:
 *           $ref: '#/definitions/User'
 */
/**
 * @swagger
 * /users/validate:
 *   get:
 *     tags:
 *       - users
 *     description: Validate account for user
 *     produces:
 *       - application/json
 *     responses:
 *       201:
 *         description: User validate
 */
router
    .get('/users/validate', userController.validateAccount)
    .get('/users/:username', userController.getUser)
    .post('/users', userController.register);


module.exports = router;
