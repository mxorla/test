const express = require('express');
const userRoutes = require('./user.routes');
const inviteRoutes = require('./invite.routes');

const router = new express.Router();
router
    .use('/api',
        userRoutes,
        inviteRoutes);



module.exports = router;
