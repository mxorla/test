const express = require('express');
const inviteController = require('../../controllers/invite.controller');

const router = new express.Router();
router
    .post('/invite', inviteController.addInvite);

module.exports = router;
