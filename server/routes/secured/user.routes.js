const express = require('express');
const userController = require('../../controllers/user.controller');

const router = new express.Router();
router
    .post('/users/geolocation', userController.setGeolocation);

module.exports = router;
