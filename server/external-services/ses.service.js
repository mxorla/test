const AWS = require('aws-sdk');

const SES = new AWS.SES({
    apiVersion: '2010-12-01',
    accessKeyId: process.env.AWS_SES_ACCESS_KEY_ID,
    secretAccessKei: process.env.AWS_SES_SECRET_ACCESS_KEY,
    region: process.env.AWS_SES_REGION
});

class SESExternalService {
    async sendEmail(to, subject, body) {
        var params = {
            Source: process.env.EMAIL_SOURCE,
            Destination: {
                ToAddresses: Array.isArray(to) ? to : [to]
            },
            Message: {
                Subject: {
                    Charset: 'UTF-8',
                    Data: subject
                },
                Body: {
                    Html: {
                        Charset: 'UTF-8',
                        Data: body
                    }
                }
            }
        };

        return SES.sendEmail(params).promise();
    }
}

module.exports = new SESExternalService;