const firebase = require('firebase-admin');

const serviceAccount = require('./enter-seg-dev-firebase-adminsdk.json');

// The Firebase token of the device which will get the notification
// It can be a string or an array of strings

firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: 'https://enter-seg-dev.firebaseio.com'
});
class FirebaseExternalService {
    async sendPush(toPushId, title, body) {
        const payload = {
            notification: {
                title,
                body
            }
        };

        const options = {
            priority: 'high',
            timeToLive: 60 * 60 * 24, // 1 day
        };
        // type MessagingOptions = {
        //     dryRun?: boolean;
        //     priority?: string; // normal or high
        //     timeToLive?: number; // in second
        //     collapseKey?: string;
        //     mutableContent?: boolean;
        //     contentAvailable?: boolean;
        //     restrictedPackageName?: string;
        //     [key: string]: any | undefined;
        //   };

        firebase.messaging().sendToDevice(toPushId, payload, options);
    }
}

module.exports = new FirebaseExternalService;