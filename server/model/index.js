const userSchema = require('./config').schemas.userSchema,
    roleSchema = require('./config').schemas.roleSchema,
    inviteSchema = require('./config').schemas.inviteSchema;

const models = {
    User: new userSchema(),
    Invite: new inviteSchema(),
    Role: roleSchema
};

module.exports = models;