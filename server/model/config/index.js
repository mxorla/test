
var mysql = require('mysql');
const userSchema = require('../UserSchema');
const inviteSchema = require('../InviteSchema');

var pool = mysql.createPool({
    host: process.env.RDS_HOSTNAME,
    user: process.env.RDS_USERNAME,
    database: process.env.RDS_DB_NAME,
    password: process.env.RDS_PASSWORD,
    port: process.env.RDS_PORT
});

const exec = (query) => {
    return new Promise((resolve, reject) => {
        pool.getConnection(function (err, connection) {
            if (err) reject(err); // not connected!

            // Use the connection
            connection.query(query, function (error, results) {
                // When done with the connection, release it.
                connection.release();

                // Handle error after the release.
                if (error) reject(error);

                resolve(results);
            });
        });
    });
};


const schemas = {
    userSchema,
    inviteSchema,
    roleSchema: userSchema
};

module.exports.schemas = schemas;
module.exports.exec = exec;
