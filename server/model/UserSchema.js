const model = require('./config');

class UserSchema {
    async findByUsername(username) {
        const response = await model.exec(`Select *, u.name as name, r.name as role from 
        users u  inner join user_in_roles uir
         on u.id = uir.user_id  
         inner join roles r on r.id = uir.role_id where username="${username}"`);
        return response[0];
    }

    async upsertPushId(username, pushId) {
        try {
            await model.exec(`INSERT INTO user_devices
            (user_id, push_id, created_at) 
            VALUES((SELECT id from users where username="${username}"), "${pushId}", CURRENT_TIMESTAMP);`);
        } catch (error) {
            if (error.code !== 'ER_DUP_ENTRY')
                throw error;
        }
    }

    async getDevices(username) {
        const response = await model.exec(`Select * from user_devices where user_id=(select id from users where username="${username}")`);
        return response;
    }

    async getLocations(username) {
        const response = await model.exec(`Select * from user_locations where user_id=(select id from users where username="${username}")`);
        return response;
    }

    async setGeolocation(username, lat, lng) {
        await model.exec(`INSERT INTO user_locations
            (user_id, lat, lng, favorite) 
            VALUES((SELECT id from users where username="${username}"), ${lat}, ${lng}, true);`);
    }

}
module.exports = UserSchema;