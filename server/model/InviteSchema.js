const model = require('./config');
const bcrypt = require('bcryptjs');
const saltRounds = 10;

class InviteSchema {
    async findByUsername(username) {
        const response = await model.exec(`Select *, u.name as name, r.name as role from 
        users u  inner join user_in_roles uir
         on u.id = uir.user_id  
         inner join roles r on r.id = uir.role_id where username="${username}"`);
        return response[0];
    }

    async saveInvite(username, { datetime, email, description, tolerance, phone }) {
        const hashvalue = await bcrypt.hash(datetime + email + description + tolerance + phone, saltRounds);
        const invite = await model.exec(`INSERT INTO invites
        (user_id, datetime, email_invited, description, tolerance, phone, hashvalue) 
        VALUES((SELECT id from users where username="${username}"), "${datetime}", "${email}",
        "${description}", "${tolerance}","${phone}", "${hashvalue}");`);
        return { invite, hashvalue };
    }

    async changeStatus(inviteId) {
        const invite = await model.exec(`UPDATE invites SET
        status = "PENDING_FORM" where id=${inviteId};`);
        return { invite };
    }
}
module.exports = InviteSchema;