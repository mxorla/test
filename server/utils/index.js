const moment = require('moment');

const getUtcDate = (date, format = null) => {
    const utcDate = moment(date).utc();
    if (/^[\d]{4}-[\d]{1,2}-[\d]{1,2}$/.test(date))
        return utcDate.format("YYYY-MM-DD");
    if (format)
        return utcDate.format(format);
    return utcDate.toDate();

}

module.exports = getUtcDate;
