FROM node:10.15.3-alpine


RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY ./ /usr/src/app/

RUN npm install --production

ENV PORT 3000
EXPOSE 3000

CMD [ "npm", "start" ]
